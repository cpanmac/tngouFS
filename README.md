#TngouFS 文件服务系统

TngouFS是天狗网（[www.tngou.net](http://www.tngou.net)） 用于 该网站的图片存储。


### 主要功能


1. 上传图片
1. 基于URL直接下载图片
1. 在线剪切图片
1. 支持百度的UEditor后台处理

### 网站功能
独有的图片网站 DEMO ： [www.tngou.net/tnfs](http://www.tngou.net/tnfs)


1. 图片分类
1. 图片抓取
1. 图片展示


### 技术

网站后台主要是由JAVA 编写，其中图片文件的处理应用的百度的UEditor的JAVA后端代码，WEB方面用的是Servlet3.0+FreeMarker编辑，
界面UI使用的的妹子Amaze UI编写，数据库用的是MySQL，总结一下主要技术

druid-数据库连接池
dbutils-简单的数据库对象
jsoup-数据爬虫+HTML提取
servlet+freemarker-JAVA WEB框架
Amaze UI 界面框架

其实技术不多，，好多都是一些基本的JAVA方法。


### 文档

文档地址：[www.tngou.net/doc/tnfs](http://www.tngou.net/doc/tnfs) 官方文档，可以了解更多关于TngouFS

演示地址：[www.tngou.net/tnfs](http://www.tngou.net/tnfs)   后台没有开发，如需要后台的可以安装到本地。

图片API地址：[www.tngou.net/doc/gallery](http://www.tngou.net/doc/gallery)


### 联系我们
如果对该项目感兴趣，可以加入我们，成为开源软件的贡献者。
邮箱：tngou@tngou.net
QQ：397713572
联系人：陈磊 @tngou
联系电话：13880334484 (成都)



### 更新

2015年7月16日，发布第一个基础版本












