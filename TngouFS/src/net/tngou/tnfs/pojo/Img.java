package net.tngou.tnfs.pojo;

import java.io.Serializable;

/**
 * 图片信息
* @ClassName: Img
* @Description: TODO(这里用一句话描述这个类的作用)
* @author tngou.ceo@aliyun.com
* @date 2014年12月23日 下午4:08:13
*
 */
public class Img  implements Serializable{

	private String id; //图片标示
//	private String path ; //访问路径
	private String src ;  //原图路径
	private int width; //图片宽度，单位：像素（px）。
	private int height;//图片高度，单位：像素（px）。
	private long size; //图片大小。单位 b
	private String format; //图片类型，如png、jpeg、gif、bmp等。
	private String title;//图片标题
	private String message;//图片信息
	private String tag ;//图片分类  预留字段，用于图片上传标记
	private long time  ;//图片更新时间
	private String temp;//临时文件
	private String cuttype="tc"; //剪切方式   oc:省略剪切, fc 填充剪切  tc ：透明剪切
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
//	public String getPath() {
//		return path;
//	}
//	public void setPath(String path) {
//		this.path = path;
//	}
	public String getSrc() {
		return src;
	}
	public void setSrc(String src) {
		this.src = src;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTime() {
		return time;
	}
	public void setTime(long time) {
		this.time = time;
	}
	
	
	@Override
	public String toString() {
		String s = "id:"+id+"\n"
				+ "src:"+src+"\n"
				+ "size:"+size+"\n"
				+ "width:"+width+"\n"
				+ "height:"+height+"\n"
				+ "format:"+format+"\n"
				+ "title:"+title+"\n"
				+ "message:"+message+"\n"
				 + "tag:"+tag+"\n"
				 + "time:"+time+"\n";
		return s;
	}
	public String getTemp() {
		return temp;
	}
	public void setTemp(String temp) {
		this.temp = temp;
	}
	
	public String getCuttype() {
		return cuttype;
	}
	public void setCuttype(String cuttype) {
		this.cuttype = cuttype;
	}
	
	
	
}
