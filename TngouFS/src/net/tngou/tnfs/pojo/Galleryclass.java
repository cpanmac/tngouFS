package net.tngou.tnfs.pojo;

/**
 * 
 * @author 陈磊
 *
 */
public class Galleryclass extends POJO 
{
	private String name;
	private String title;
	private String keywords;
	private String description;
	private int seq;//排序 从0。。。。10开始
	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public int getSeq() {
		return seq;
	}
	public void setSeq(int seq) {
		this.seq = seq;
	}
	

	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	@Override
	protected boolean isObjectCachedByID() {
		
		return true;
	}
}
