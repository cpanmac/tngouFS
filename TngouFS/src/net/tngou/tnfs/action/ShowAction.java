package net.tngou.tnfs.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;






import net.tngou.tnfs.enums.CacheEnum;
import net.tngou.tnfs.enums.CountEnum;
import net.tngou.tnfs.enums.TTypeEnum;
import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.Picture;
import net.tngou.tnfs.quartz.CacheUtil;



public class ShowAction extends BaseAction {

	

	
	@Override
	public void execute() throws ServletException, IOException {
		
		
		String[] params = request.getParams();
		Gallery gallery = new Gallery().get(Long.parseLong(params[0]));
		if(gallery==null){
			run_404();
			return;
		}
		CacheUtil.getInstance(CacheEnum.VisitEh, CountEnum.count).Add(gallery.getId(), TTypeEnum.tnfs_gallery);
		root.put("gallery", gallery);
		String filter="gallery="+gallery.getId(); 
		List<?> list =  baseService.getList(filter,Picture.class);
		root.put("list", list);
		Galleryclass galleryclass = new Galleryclass().get(gallery.getGalleryclass());
		root.put("galleryclass", galleryclass);
		List<?> galleryclasses = baseService.getList("seq",OrderType.ASC,Galleryclass.class);	
		root.put("galleryclasses", galleryclasses);
		
		  filter="galleryclass="+gallery.getGalleryclass();
		  Gallery last =gallery.last(gallery.getId(),filter);//最新
		  Gallery next =gallery.next(gallery.getId(),filter);//旧的
			 root.put("last", last);
			root.put("next", next);
		
			root.put("title", gallery.getTitle()+"_"+galleryclass.getTitle()+"-天狗美阅 美图");
			 root.put("keywords", gallery.getTitle());
			 root.put("description", galleryclass.getTitle()+" "+gallery.getTitle());
		printFreemarker("default/show.ftl", root);
	}
}
