package net.tngou.tnfs.action.manage;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;

import net.tngou.tnfs.action.BaseAction;
import net.tngou.tnfs.jdbc.OrderType;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.Index;
import net.tngou.tnfs.service.BaseService;
import net.tngou.tnfs.service.GalleryService;

public class IndexAction extends BaseAction {

	
	
	@Override
	public void execute() throws ServletException, IOException {
		int page= request.getParameter("p")==null?1:Integer.parseInt(request.getParameter("p"));
		GalleryService galleryService = new GalleryService();
		Index index = galleryService.getIndex(page, 20);
		root.put("index", index);
		printFreemarker("manage/index.ftl", root);
	}
	
	
	
	@Override
	protected void printFreemarker(String ftl, Map<String, Object> root) {
		List<?> galleryclasses = baseService.getList("seq",OrderType.ASC,Galleryclass.class);	
		root.put("galleryclasses", galleryclasses);
		super.printFreemarker(ftl, root);
	}
	
	
	
}
