package net.tngou.tnfs.service;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;





import net.tngou.tnfs.dao.GalleryDao;
import net.tngou.tnfs.jdbc.Inflector;
import net.tngou.tnfs.pojo.Gallery;
import net.tngou.tnfs.pojo.Galleryclass;
import net.tngou.tnfs.pojo.Index;
import net.tngou.tnfs.pojo.Picture;
import net.tngou.tnfs.util.PageUtil;

public class GalleryService extends BaseService {

	private final  String fullyQualifiedName =  StringUtils.capitalize(Inflector.getInstance().fullyQualifiedName(Gallery.class));;
	
	public  Index getIndex(int page,int size) {
		
		Serializable key="geIndex_p"+page+"_s"+size;	
		Index index =  (Index) cacheEngine.get(fullyQualifiedName, key);
		if(index==null)
		{
			index = new Index();
			GalleryDao dao = new GalleryDao();
			index .setGallerysize(new Gallery().totalCount());
			index.setPicturesize(new Picture().totalCount());
			index.setGallerysizeweek(dao.gallerySize(7));
			index.setPicturesizeweek(dao.pictureSize(7));
			index.setPage(this.getPage(page, size, Gallery.class));
			
			cacheEngine.add(fullyQualifiedName, key, index);
		}
		
		return index;
		
	}
	
	

	public PageUtil getNewsPage(long id, int rows, int type) {
		Serializable key="getNewsPage_id"+id+"_r"+rows+"_t"+type;
		
		PageUtil page=(PageUtil) cacheEngine.get(fullyQualifiedName, key);
		if(page==null)
		{
			GalleryDao dao = new GalleryDao();
			page = new PageUtil(dao.getGallerie(id, rows, type), 1, rows, dao.getGallerie(id, type));
			cacheEngine.add(fullyQualifiedName, key, page);
		}
		
		return page;
	}
}
