package net.tngou.tnfs.quartz;

import java.util.Date;













import net.tngou.tnfs.enums.CacheEnum;
import net.tngou.tnfs.enums.CountEnum;
import net.tngou.tnfs.jdbc.DBManager;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VisitLogJob implements Job {
	private static Logger _log = LoggerFactory.getLogger(VisitLogJob.class);
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		_log.info("执行作业： "+context.getJobDetail().getKey() +"  执行时间："+new Date()+"********************************");
		
		CacheUtil.getInstance(CacheEnum.VisitEh, CountEnum.count).Update();
	
//		VisitLogEhCache.Update();
//		SearchLogEhCache.Update();
//		FavoriteEhCache.Update();
//		CommentEhCache.Update();
		DBManager.closeConnection();
		//DBManager.closeDataSource();
	}
	
}
