<#include "header.ftl">

<hr>

<div class="am-container">

 <ul class="am-avg-sm-2 am-avg-md-4 am-avg-lg-6 am-margin gallery-list">
    
    <#list page.list as item>
      <li>
        <a href="${Domain.base}/show/${item.id}">
  
          <img class="am-img-thumbnail am-img-bdrs" src="${Domain.imgurl}/image${item.img}_270x320.jpg" alt="">
               <div class="gallery-title">${item.title}</div>
           </a>
          <div class="gallery-desc">${item.size}张  &nbsp;&nbsp;浏览量 :${item.count}
</div>
      
      </li>
      </#list>
    </ul>

    <div class="am-margin am-cf">
      <hr>
      
    
    <#include "../common/page.ftl">
    
    </div>


</div>


<#include "footer.ftl">