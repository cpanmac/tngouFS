<!doctype html>
<html class="no-js">
<head>
 <title>${title}</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="${keywords}">
	<meta name="description" content="${description}">
	<meta name="author" content="${author}"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <link rel="icon" type="image/png" href="${Domain.base}/common/amazeui/i/favicon.png">
  <link rel="apple-touch-icon-precomposed" href="${Domain.base}/common/amazeui/i/app-icon72x72@2x.png">
  <meta name="apple-mobile-web-app-title" content="Amaze UI" />
  <link rel="stylesheet" href="${Domain.base}/common/amazeui/css/amazeui.min.css"/>
  <link rel="stylesheet" href="${Domain.base}/common/amazeui/css/admin.css">
    
  <script type="text/javascript" src="${Domain.base}/common/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${Domain.base}/common/js/jquery.form.min.js"></script>
  
</head>
<body>
<!--[if lte IE 9]>
<p class="browsehappy">你正在使用<strong>过时</strong>的浏览器，TngouFS 暂不支持。 请 <a href="http://browsehappy.com/" target="_blank">升级浏览器</a>
  以获得更好的体验！</p>
<![endif]-->


<header data-am-widget="header" class="am-header  am-header-default am-no-layout" style="border-bottom:1px solid #e7e7e7;background: #f9f9f9;">
 
  <div class="am-header-left am-header-nav" style="margin-left:12rem;">
   <ol class="am-breadcrumb">
  <li><a href="${Domain.base}">首页</a></li>
  <#if galleryclass??>
  <li><a href="${Domain.base}/index/${galleryclass.id}">${galleryclass.name}</a></li>
  </#if>
 &nbsp;
</ol>

  </div>
  <section class="am-g am-hide-md-down am-margin">
  <div class="am-header-right am-header-nav" >
  	 <a href="${Domain.base}" class="am-btn am-btn-warning">最新图片
	    <i class="am-header-icon"></i>
	 </a>
   
   <#if user??> 
   <a href="${Domain.manage}" class="am-btn am-btn-primary">后台管理
	    <i class="am-header-icon"></i>
	 </a>
	 <#else>
	 <a href="${Domain.base}/login" class="am-btn am-btn-primary">登录	    
	 </a>
   </#if>
    <a href="http://www.tngou.net/doc/gallery" class="am-btn am-btn-secondary" target="_blank">API开放接口	  
	 </a>	 
	  <a href="http://www.tngou.net/doc/tnfs" class="am-btn am-btn-success" target="_blank">网站源码	  
	 </a>
  </div>
  </section>
</header>




<div data-am-widget="navbar" class="am-navbar am-show-lg-only am-cf am-no-layout"  style="height:100%;width:80px;border-right:1px solid #e7e7e7;background: #f9f9f9;">
  <ul class="am-navbar-nav am-cf am-avg-sm-1" style="height:100%;">
    <li id="quickPan">
      <a href="${Domain.base}" style="display:block;">
        <span class="am-icon-home am-text-warning"></span>
        <span class="am-navbar-label am-text-warning">主页</span>
      </a>   
      <#list galleryclasses as item>
       <a href="${Domain.base}/index/${item.id}" style="display:block;">
        <span class="am-icon-caret-up am-icon-sm"></span>
        <span class="am-navbar-label">${item.name}</span>
      </a>
      </#list>
    </li>    
</ul></div>






