<#include "header.ftl">

  <!-- content start -->
  <div class="admin-content">

    <div class="am-cf am-padding">
      <div class="am-fl am-cf"><strong class="am-text-primary am-text-lg">首页</strong> / <small>一些常用模块</small></div>
    </div>

    <ul class="am-avg-sm-1 am-avg-md-4 am-margin am-padding am-text-center admin-content-list ">
      <li><a href="#" class="am-text-success"><span class="am-icon-btn am-icon-image"></span><br/>新增图片<br/>${index.picturesizeweek}</a></li>
      <li><a href="#" class="am-text-warning"><span class="am-icon-btn am-icon-folder"></span><br/>新增图库<br/>${index.gallerysizeweek}</a></li>
      <li><a href="#" class="am-text-danger"><span class="am-icon-btn am-icon-file-image-o"></span><br/>总图片<br/>${index.picturesize}</a></li>
      <li><a href="#" class="am-text-secondary"><span class="am-icon-btn am-icon-folder-open"></span><br/>总图库<br/>${index.gallerysize}</a></li>
    </ul>

    <div class="am-g">
      <div class="am-u-sm-12">
        <table class="am-table am-table-bd am-table-striped admin-content-table">
          <thead>
          <tr>
            <th>分类</th>
			<th>图库名称</th>
            <th>图片</th>
                <th>时间</th>
            <th>管理</th>
          </tr>
          </thead>
          <tbody>
          
          <#list index.page.list as item>
          <tr><td><a href="${Domain.manage}/gallery/list/${item.galleryclass}">


<#list galleryclasses as itemc>
<#if item.galleryclass==itemc.id>
           ${itemc.name}
</#if>
 </#list>
</a></td><td><a href="${Domain.manage}/gallery/show/${item.id}">${item.title}</a></td>
          <td><span class="am-badge am-badge-success">${item.size}</span></td>
			<td>${item.time}</td>
            <td>
              <div class="am-dropdown" data-am-dropdown>
                <button class="am-btn am-btn-default am-btn-xs am-dropdown-toggle" data-am-dropdown-toggle><span class="am-icon-cog"></span> <span class="am-icon-caret-down"></span></button>
                <ul class="am-dropdown-content">
                  <li><a href="${Domain.manage}/gallery/edit?id=${item.id}">编辑</a></li>
                  <li><a href="${Domain.manage}/gallery/show/${item.id}">查看</a></li>

                </ul>
              </div>
            </td>
          </tr>
         </#list>
          </tbody>
        </table>
<#include "../common/page.ftl">
      </div>
    </div>

  
  </div>
  <!-- content end -->

<#include "footer.ftl">
